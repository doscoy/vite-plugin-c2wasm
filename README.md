# vite-plugin-c2wasm

c,c++のソースファイルを wasm にコンパイルしてロードするプラグイン

## install

```console
npm i gitlab:doscoy/vite-plugin-c2wasm -D
```

## example

```
git clone https://gitlab.com/doscoy/vite-plugin-c2wasm.git
cd vite-plugin-c2wasm
npm install
npm run example
```

## usage

```js
// vite.config.js

import { defineConfig } from "vite";
import c2wasm from "vite-plugin-c2wasm";

export default defineConfig({
  plugins: [
    c2wasm({
      clangPath: "clang",
      ldPath: "wasm-ld",
      cFlags: ["-std=c99", "-O3"],
      cppFlags: ["-std=c++17", "-O3"],
      linkFlags: ["-s", "-O3"],
      injectConsoleFn: true,
    }),
  ],
});
```
```json
// tsconfig.json

{
  "compilerOptions": {
    "types": [
      "vite-plugin-c2wasm/dist/module"
    ]
  }
}
```
```cpp
// example.cpp

__attribute__((import_name("jsFn")))
void CallJsFn();

__attribute__((export_name("add")))
int Add(int a, int b) {
  CallJsFn();
  return a + b;
}
```
```js
// example.js

import init from './example.cpp?init';

const imports = { env: { jsFn: () => console.log("from wasm!!") } };

init(imports).then((instance) => {
  instance.exports.add(1, 1);
});
```

その他はexampleを参照

## 参考

- [WebAssembly lld port](https://lld.llvm.org/WebAssembly.html)