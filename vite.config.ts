import { defineConfig } from "vite";
import c2wasm from "./src";

export default defineConfig({
  plugins: [
    c2wasm({
      clangPath: "/usr/local/opt/llvm/bin/clang",
      ldPath: "/usr/local/opt/llvm/bin/wasm-ld",
      cppFlags: ["-std=c++17", "-O3", "-DDEBUG"],
      linkFlags: ["-s", "-O3"],
      injectConsoleFn: true,
    }),
  ],
});
