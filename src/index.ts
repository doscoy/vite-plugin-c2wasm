import { type Plugin } from "vite";

import { relative, resolve } from "node:path";
import { mkdir, readFile, stat } from "node:fs/promises";
import { execFile, type ExecFileException } from "node:child_process";

interface WASMConfig {
  clangPath: string;
  ldPath: string;
  cFlags: string[];
  cppFlags: string[];
  linkFlags: string[];
  injectConsoleFn: boolean;
}

const PLUGIN_NAME = "vite-plugin-c2wasm";

const cRE = /\.c\b/;
const ccRE = /\.cpp\b|\.cc\b/;
const urlRE = /\?url$/;
const binaryRE = /\?binary$/;
const initRE = /[?&]init/;
const syncRE = /[?&]sync/;

function getPath(id: string): string {
  const paramPos = id.indexOf("?");
  return paramPos !== -1 ? id.slice(0, paramPos) : id;
}

function getDir(id: string): string {
  const path = getPath(id);
  return path.slice(0, path.lastIndexOf("/") + 1);
}

function getFileName(id: string): string {
  const path = getPath(id);
  return path.slice(path.lastIndexOf("/") + 1);
}

function getStem(id: string): string {
  const fileName = getFileName(id);
  return fileName.slice(0, fileName.lastIndexOf("."));
}

function execFileWrap<T>(cmd: string, args: string[], output: T) {
  return new Promise<{ output?: T; msg: string }>((resolve) => {
    execFile(
      cmd,
      args,
      undefined,
      (
        err: ExecFileException | null,
        stdout: string | Buffer,
        stderr: string | Buffer,
      ): void => {
        resolve({
          output: err === null ? output : undefined,
          msg: (stdout as string) + (stderr as string),
        });
      },
    );
  });
}

function compile(cmd: string, input: string, flags: string[], outDir: string) {
  const stem = getStem(input);
  const output = `${outDir}/${stem}.o`;
  const args = [
    ...[input, "-c", "--target=wasm32", "-nostdlib", "-fvisibility=hidden"],
    ...flags,
    ...["-o", output],
  ];
  return execFileWrap(cmd, args, output);
}

function link(cmd: string, input: string, flags: string[], outDir: string) {
  const stem = getStem(input);
  const output = `${outDir}/${stem}.wasm`;
  const args = [...[input, "--no-entry"], ...flags, ...["-o", output]];
  return execFileWrap(cmd, args, output);
}

function genBase64ToBinFn(varName: string, base64: string): string {
  return [
    `const ${varName} = (base64 => {`,
    '  if (typeof Buffer === "function" && typeof Buffer.from === "function") {',
    '    return new Uint8Array(Buffer.from(base64, "base64").buffer);',
    '  } else if (typeof atob === "function") {',
    "    return Uint8Array.from(atob(base64), c => c.charCodeAt(0));",
    "  } else {",
    '    throw new Error("Failed to decode base64-encoded data URL, Buffer and atob are not supported");',
    "  };",
    `})("${base64}");`,
  ].join("");
}

function genConsoleFn(
  tgtFile: string,
  instanceName: string,
  functionName: string,
): string {
  return [
    `function ${functionName}(pos, len) {`,
    "  console.log(",
    `    "[${tgtFile}]"+`,
    "    `[${performance.now().toFixed(3)}]`+",
    "    new TextDecoder().decode(",
    `      new DataView(${instanceName}.exports.memory.buffer, pos, len)`,
    "    )",
    "  );",
    "}",
  ].join("");
}

export default function wasm({
  clangPath = "clang",
  ldPath = "wasm-ld",
  cFlags = [],
  cppFlags = [],
  linkFlags = [],
  injectConsoleFn = false,
}: Partial<WASMConfig> = {}): Plugin {
  let buildDir: string = resolve(
    resolve("./"),
    "node_modules",
    `.${PLUGIN_NAME}`,
  );

  return {
    name: PLUGIN_NAME,
    enforce: "pre",

    async load(id) {
      const [compilerPath, compileFlags] = (() => {
        if (cRE.test(id)) {
          return [clangPath, cFlags];
        } else if (ccRE.test(id)) {
          return [clangPath + "++", cppFlags];
        } else {
          return [null, []];
        }
      })();
      if (compilerPath === null) return;

      const tgtFile = relative(resolve("./"), getPath(id));
      const outDir = resolve(buildDir, relative(resolve("./"), getDir(id)));
      await mkdir(outDir, { recursive: true });

      const objFile = await compile(
        compilerPath,
        getPath(id),
        compileFlags,
        outDir,
      );
      if (!objFile.output) return this.error(`${objFile.msg}`);
      if (objFile.msg) this.warn(objFile.msg);

      const wasmFile = await link(ldPath, objFile.output, linkFlags, outDir);
      if (!wasmFile.output) return this.error(`${wasmFile.msg}`);
      if (wasmFile.msg) this.warn(wasmFile.msg);

      if (urlRE.test(id)) {
        return `export { default } from "${wasmFile.output}?url";`;
      }

      if (binaryRE.test(id)) {
        const base64 = await readFile(wasmFile.output).then((bin) =>
          bin.toString("base64"),
        );
        return [genBase64ToBinFn("bin", base64), "export default bin;"].join(
          "\n",
        );
      }

      if (initRE.test(id)) {
        if (syncRE.test(id)) {
          const { size } = await stat(wasmFile.output);
          if (size >= 4096) {
            const tgt = relative(buildDir, wasmFile.output);
            this.warn(
              `${tgt} is larger than 4KB. Synchronous initialization fails on the main thread.`,
            );
          }
          const base64 = await readFile(wasmFile.output).then((bin) =>
            bin.toString("base64"),
          );
          if (injectConsoleFn) {
            return [
              `${genBase64ToBinFn("bin", base64)}`,
              "export default function(opts = {}) {",
              "  const instance = new WebAssembly.Instance(new WebAssembly.Module(bin), { ...{ env: { console: printStr } }, ...opts });",
              `  ${genConsoleFn(tgtFile, "instance", "printStr")}`,
              "  return instance;",
              "};",
            ].join("\n");
          }

          return [
            `${genBase64ToBinFn("bin", base64)}`,
            "export default function(opts = {}) {",
            "  return new WebAssembly.Instance(new WebAssembly.Module(bin), opts);",
            "};",
          ].join("\n");
        }

        if (injectConsoleFn) {
          return [
            `import init from "${wasmFile.output}?init";`,
            "export default async function(opts = {}) {",
            " const instance = await init({ ...{ env: { console: printStr } }, ...opts });",
            ` ${genConsoleFn(tgtFile, "instance", "printStr")}`,
            " return instance;",
            "}",
          ].join("\n ");
        }

        return `export { default } from "${wasmFile.output}?init";`;
      }
    },
  };
}
