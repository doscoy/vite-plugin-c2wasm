declare module "*?binary" {
  const bin: Uint8Array;
  export default bin;
}

type InitWasmFn = (opts?: WebAssembly.Imports) => Promise<WebAssembly.Instance>;

declare module "*.c?init" {
  const initWasm: InitWasmFn;
  export default initWasm;
}

declare module "*.cc?init" {
  const initWasm: InitWasmFn;
  export default initWasm;
}

declare module "*.cpp?init" {
  const initWasm: InitWasmFn;
  export default initWasm;
}

type InitWasmSyncFn = (opts?: WebAssembly.Imports) => WebAssembly.Instance;

declare module "*.c?init&sync" {
  const initWasm: InitWasmSyncFn;
  export default initWasm;
}

declare module "*.cc?init&sync" {
  const initWasm: InitWasmSyncFn;
  export default initWasm;
}

declare module "*.cpp?init&sync" {
  const initWasm: InitWasmSyncFn;
  export default initWasm;
}
