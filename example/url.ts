import url from "./example.cpp?url";
import type { ExampleInstance } from "./example";

let wasm: ExampleInstance;

const imports: WebAssembly.Imports = {env: {console: (pos:number, len:number) => {
  const strBin = new Uint8Array(wasm.exports.memory.buffer, pos, len);
  const decoder = new TextDecoder();
  console.log(decoder.decode(strBin));
}}}

WebAssembly.instantiateStreaming(fetch(url), imports).then((src) => {
  console.log("wasm url =", url);
  wasm = src.instance as ExampleInstance;
  console.log("1 + 2 =", wasm.exports.add(1, 2));
  console.log("heap base =", wasm.exports.getHeapBase());
  console.log("memory size =", wasm.exports.memory.buffer.byteLength);
  wasm.exports.prepareHeap(0x10000 * 10);
  console.log("memory size =", wasm.exports.memory.buffer.byteLength);
});
