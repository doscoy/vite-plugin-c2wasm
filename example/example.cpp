#include <stddef.h>
#include <stdint.h>
#include <wasm_simd128.h>

#define IMPORT(name) __attribute__((import_name(name)))
#define EXPORT(name) __attribute__((export_name(name)))

#ifdef DEBUG
IMPORT("console") void console(const char *data, size_t size);
template <size_t N> void debug(const char (&str)[N]) {
  console(&str[0], N - 1);
}
#else
template <size_t N> void debug(const char (&)[N]) {
  // do nothing
}
#endif

static inline constexpr auto kPageSize{0x10000};

extern uint8_t __heap_base;
EXPORT("getHeapBase") void *GetHeapBase() {
  debug(__func__);
  return &__heap_base;
}

EXPORT("prepareHeap") void PrepareHeap(int needs) {
  debug(__func__);
  // see "BuiltinsWebAssembly.def"
  const auto has{__builtin_wasm_memory_size(0) * kPageSize -
                 reinterpret_cast<intptr_t>(&__heap_base)};
  const auto not_enough{needs - has};
  if (not_enough < 0)
    return;
  const auto add_page{(not_enough + kPageSize - 1) / kPageSize};
  __builtin_wasm_memory_grow(0, add_page);
}

EXPORT("add") int Add(int a, int b) {
  debug(__func__);
  return a + b;
}
