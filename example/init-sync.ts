import make from "./example.cpp?init&sync";
import type { ExampleInstance } from "./example";

const wasm = make() as ExampleInstance;
console.log("1 + 2 =", wasm.exports.add(1, 2));
console.log("heap base =", wasm.exports.getHeapBase());
console.log("memory size =", wasm.exports.memory.buffer.byteLength);
wasm.exports.prepareHeap(0x10000 * 10);
console.log("memory size =", wasm.exports.memory.buffer.byteLength);
