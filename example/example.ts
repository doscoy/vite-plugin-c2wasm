export interface ExampleInstance extends WebAssembly.Instance {
  exports: {
    memory: WebAssembly.Memory;
    add: (a: number, b: number) => number;
    getHeapBase(): () => number;
    prepareHeap(needs: number): () => number;
  };
}