import bin from "./example.cpp?binary";
import type { ExampleInstance } from "./example";

console.log("WebAssembly.validate =", WebAssembly.validate(bin));

const wasmModule = new WebAssembly.Module(bin);
const wasm = new WebAssembly.Instance(wasmModule, {
  env: {
    console: (pos: number, len: number) => {
      const strBin = new Uint8Array(wasm.exports.memory.buffer, pos, len);
      const decoder = new TextDecoder();
      console.log(decoder.decode(strBin));
    },
  },
}) as ExampleInstance;

console.log("1 + 2 =", wasm.exports.add(1, 2));
console.log("heap base =", wasm.exports.getHeapBase());
console.log("memory size =", wasm.exports.memory.buffer.byteLength);
wasm.exports.prepareHeap(0x10000 * 10);
console.log("memory size =", wasm.exports.memory.buffer.byteLength);
